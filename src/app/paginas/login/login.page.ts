import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  async loginGoogle() {
    const error = this.auth.googleLogin();
    if (error === undefined) {
      alert(JSON.stringify(error));
    } else {
      this.router.navigate(['home']);
    }
  }

}
