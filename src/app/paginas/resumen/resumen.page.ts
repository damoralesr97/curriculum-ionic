import { Component, OnInit } from '@angular/core';
import { DatosPersonales } from '../../interfaces/datos-personales';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.page.html',
  styleUrls: ['./resumen.page.scss'],
})
export class ResumenPage implements OnInit {

  datos = new DatosPersonales();

  constructor(public route: ActivatedRoute, public router: Router) { }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.datos = this.router.getCurrentNavigation().extras.state.datos;
      }
    });
  }

  goHome() {
    this.router.navigate(['home']);
  }

}
