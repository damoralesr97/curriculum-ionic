import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage implements OnInit {

  constructor(private iab: InAppBrowser, private vibration: Vibration) { }

  ngOnInit() {
  }

  info() {
    this.vibration.vibrate(2000);
    const url = 'https://www.modelos-de-curriculum.com/hacer-un-curriculum-vitae/';
    this.iab.create(url, '_self');
  }

}
