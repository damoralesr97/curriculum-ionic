import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { DatosPersonales } from '../../interfaces/datos-personales';
import { CurriculumService } from '../../services/curriculum.service';

@Component({
  selector: 'app-estudios',
  templateUrl: './estudios.page.html',
  styleUrls: ['./estudios.page.scss'],
})
export class EstudiosPage implements OnInit {

  datos = new DatosPersonales();

  constructor(public route: ActivatedRoute, public router: Router, private curriculumSrv: CurriculumService) { }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.datos = this.router.getCurrentNavigation().extras.state.datos;
      }
    });
  }

  onSubmitTemplateEst() {
    this.curriculumSrv.saveCurriculum(this.datos);
    const extras: NavigationExtras = {
      state: {
        datos: this.datos,
      }
    };
    this.router.navigate(['resumen'], extras);
  }

}
