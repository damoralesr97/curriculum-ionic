import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { DatosPersonales } from '../../interfaces/datos-personales';

@Component({
  selector: 'app-experiencia-laboral',
  templateUrl: './experiencia-laboral.page.html',
  styleUrls: ['./experiencia-laboral.page.scss'],
})
export class ExperienciaLaboralPage implements OnInit {

  datos = new DatosPersonales();

  constructor(public route: ActivatedRoute, public router: Router) { }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.datos = this.router.getCurrentNavigation().extras.state.datos;
      }
    });
  }

  onSubmitTemplate() {

    const extras: NavigationExtras = {
      state: {
        datos: this.datos,
      }
    };
    this.router.navigate(['estudios'], extras);
  }

}
