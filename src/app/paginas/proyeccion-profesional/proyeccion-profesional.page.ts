import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { DatosPersonales } from '../../interfaces/datos-personales';

@Component({
  selector: 'app-proyeccion-profesional',
  templateUrl: './proyeccion-profesional.page.html',
  styleUrls: ['./proyeccion-profesional.page.scss'],
})
export class ProyeccionProfesionalPage implements OnInit {

  datos = new DatosPersonales();

  constructor(public route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.datos = this.router.getCurrentNavigation().extras.state.datos;
      }
    });
  }

  onSubmitTemplate() {

    const extras: NavigationExtras = {
      state: {
        datos: this.datos,
      }
    };
    this.router.navigate(['estudios'], extras);
  }

}
