import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProyeccionProfesionalPage } from './proyeccion-profesional.page';

const routes: Routes = [
  {
    path: '',
    component: ProyeccionProfesionalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProyeccionProfesionalPageRoutingModule {}
