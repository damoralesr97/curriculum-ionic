import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProyeccionProfesionalPageRoutingModule } from './proyeccion-profesional-routing.module';

import { ProyeccionProfesionalPage } from './proyeccion-profesional.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProyeccionProfesionalPageRoutingModule
  ],
  declarations: [ProyeccionProfesionalPage]
})
export class ProyeccionProfesionalPageModule {}
