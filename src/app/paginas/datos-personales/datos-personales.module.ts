import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatosPersonalesPageRoutingModule } from './datos-personales-routing.module';

import { DatosPersonalesPage } from './datos-personales.page';
import { ImageUploadComponent } from '../../components/image-upload/image-upload.component';
import { GpsComponent } from '../../components/gps/gps.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatosPersonalesPageRoutingModule
  ],
  declarations: [DatosPersonalesPage, ImageUploadComponent, GpsComponent],
  exports: [ImageUploadComponent, GpsComponent]
})
export class DatosPersonalesPageModule {}
