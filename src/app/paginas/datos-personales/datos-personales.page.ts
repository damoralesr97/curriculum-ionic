import { Component, OnInit } from '@angular/core';
import { DatosPersonales } from '../../interfaces/datos-personales';
import { NavigationExtras, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.page.html',
  styleUrls: ['./datos-personales.page.scss'],
})
export class DatosPersonalesPage implements OnInit {

  datos = new DatosPersonales();
  experiencia: string;

  constructor(public router: Router, private toastController: ToastController) { }

  ngOnInit() {
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  onSubmitTemplate() {
    const extras: NavigationExtras = {
      state: {
        datos: this.datos,
      }
    };

    if (this.experiencia === 'si') {
      this.router.navigate(['experiencia-laboral'], extras);
    } else if  (this.experiencia === 'no') {
      this.router.navigate(['proyeccion-profesional'], extras);
    } else {
      this.presentToast('¿Tiene experiencia laboral?');
    }
  }

  imagenCargada(e) {
    console.log(JSON.stringify(e));
    this.datos.image = e;
  }


  ubicacionCargada(e) {
    console.log(JSON.stringify(e));
  }
}
