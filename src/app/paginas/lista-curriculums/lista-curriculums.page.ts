import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CurriculumService } from '../../services/curriculum.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-curriculums',
  templateUrl: './lista-curriculums.page.html',
  styleUrls: ['./lista-curriculums.page.scss'],
})
export class ListaCurriculumsPage implements OnInit {

  curriculums: Observable<any[]>;

  constructor(private curriculumService: CurriculumService, private router: Router) { }

  ngOnInit() {
    this.curriculums = this.curriculumService.getCurriculums();
    this.curriculums.subscribe(data => console.log(data));
  }

  showCurriculum(id: any) {
    this.router.navigate([`curriculum/${id}`]);
  }

  trackByFn(index, obj) {
    return obj.uid;
  }

}
