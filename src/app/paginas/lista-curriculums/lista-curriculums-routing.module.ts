import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaCurriculumsPage } from './lista-curriculums.page';

const routes: Routes = [
  {
    path: '',
    component: ListaCurriculumsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaCurriculumsPageRoutingModule {}
