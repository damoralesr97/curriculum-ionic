import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaCurriculumsPageRoutingModule } from './lista-curriculums-routing.module';

import { ListaCurriculumsPage } from './lista-curriculums.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListaCurriculumsPageRoutingModule
  ],
  declarations: [ListaCurriculumsPage]
})
export class ListaCurriculumsPageModule {}
