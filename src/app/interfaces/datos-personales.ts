export class DatosPersonales {

    uid: string;

    cedula: string;
    nombres: string;
    apellidos: string;
    direccion: string;
    fechaNac: string;
    sexo: string;

    image: {};

    cargo1: string;
    funcion1: string;
    tiempo1: string;
    cargo2: string;
    funcion2: string;
    tiempo2: string;

    proyeccion: string;

    titulo3: string;
    universidad3: string;
    anio3: string;
    titulo4: string;
    universidad4: string;
    anio4: string;

}
