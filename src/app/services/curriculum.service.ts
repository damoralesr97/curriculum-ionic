import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { DatosPersonales } from '../interfaces/datos-personales';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CurriculumService {

  constructor(private afs: AngularFirestore) { }

  getCurriculums(): Observable<any[]> {
    return this.afs.collection('curriculum').valueChanges();
  }

  saveCurriculum(datos: DatosPersonales) {
    console.log('hola');
    const refCurriculum = this.afs.collection('curriculum');
    datos.uid = this.afs.createId();
    const param = JSON.parse(JSON.stringify(datos));
    refCurriculum.doc(datos.uid).set(param, {merge: true});
  }

  getCurriculum(uid: string): Observable<any> {
    const itemDoc = this.afs.doc<any>(`curriculum/${uid}`);
    return itemDoc.valueChanges();
  }

}
