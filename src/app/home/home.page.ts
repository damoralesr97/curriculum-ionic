import { Component } from '@angular/core';
import { PopoverController, iosTransitionAnimation, MenuController } from '@ionic/angular';
import { PopmenuComponent } from '../components/popmenu/popmenu.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private menuuController: MenuController, public popoverController: PopoverController) {}

  slides = [
    {
      img: 'assets/img/curriculum1.jpg',
      titulo: 'Genera tu curriculum<br>con pocos pasos.'
    },
    {
      img: 'assets/img/curriculum2.jpg',
      titulo: 'Recuerda llenar todos<br>los campos.'
    }
  ];

  toogleMenu() {
    this.menuuController.toggle();
  }

  //Popover
  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopmenuComponent,
      event: ev,
      mode: 'ios',
      //backdropDismiss: false,
      translucent: true
    });
    return await popover.present();
  }

  

}
