import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popmenu',
  templateUrl: './popmenu.component.html',
  styleUrls: ['./popmenu.component.scss'],
})
export class PopmenuComponent implements OnInit {

  items = Array(2);

  constructor(public router: Router, private popoverControl: PopoverController) { }

  ngOnInit() {}

  acercade() {
    this.router.navigate(['about-us']);
    this.popoverControl.dismiss();
  }

  dismissPopover() {
    if (this.popoverControl) {
      this.popoverControl.dismiss().then(() => { this.popoverControl = null; });
    }
  }

}
