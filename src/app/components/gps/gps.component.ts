import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-gps',
  templateUrl: './gps.component.html',
  styleUrls: ['./gps.component.scss'],
})
export class GpsComponent implements OnInit {

  @Output() gpsFinished = new EventEmitter<any>();

  constructor(private geolocation: Geolocation) { }

  ngOnInit() {}

  ubicacionActual() {
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
     }).catch((error) => {
       console.log('Error getting location', error);
     });

    const watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
      console.log('Ubicacion obtenida');
      this.gpsFinished.emit(data.coords.latitude);
      this.gpsFinished.emit(data.coords.longitude);
    });
  }

}
