import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'about-us',
    loadChildren: () => import('./paginas/about-us/about-us.module').then( m => m.AboutUsPageModule)
  },
  {
    path: 'datos-personales',
    loadChildren: () => import('./paginas/datos-personales/datos-personales.module').then( m => m.DatosPersonalesPageModule)
  },
  {
    path: 'experiencia-laboral',
    loadChildren: () => import('./paginas/experiencia-laboral/experiencia-laboral.module').then( m => m.ExperienciaLaboralPageModule)
  },
  {
    path: 'proyeccion-profesional',
    // tslint:disable-next-line: max-line-length
    loadChildren: () => import('./paginas/proyeccion-profesional/proyeccion-profesional.module').then( m => m.ProyeccionProfesionalPageModule)
  },
  {
    path: 'estudios',
    loadChildren: () => import('./paginas/estudios/estudios.module').then( m => m.EstudiosPageModule)
  },
  {
    path: 'resumen',
    loadChildren: () => import('./paginas/resumen/resumen.module').then( m => m.ResumenPageModule)
  },
  {
    path: 'lista-curriculums',
    loadChildren: () => import('./paginas/lista-curriculums/lista-curriculums.module').then( m => m.ListaCurriculumsPageModule)
  },
  {
    path: 'curriculum/:id',
    loadChildren: () => import('./paginas/curriculum/curriculum.module').then( m => m.CurriculumPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./paginas/registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./paginas/login/login.module').then( m => m.LoginPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
