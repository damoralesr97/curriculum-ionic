// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCqdWpNeTD8KzDdEEDXsiZriVxTzDpEHsQ',
    authDomain: 'empleos-8a83b.firebaseapp.com',
    databaseURL: 'https://empleos-8a83b.firebaseio.com',
    projectId: 'empleos-8a83b',
    storageBucket: 'empleos-8a83b.appspot.com',
    messagingSenderId: '1025056115947',
    appId: '1:1025056115947:web:d382362924241ba95f02ae'
  },
  googleWebClientId: '1025056115947-3d7pmea51t9358nnu29r2opirhgh6b9q.apps.googleusercontent.com'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
