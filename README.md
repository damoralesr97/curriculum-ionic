Geolocalizacion

Deberemos dirigirnos al directorio de nuestro proyecto dentro de la termian, una vez que estemos ahí deberemos ingresar las siguientes lineas para realizar la descarga e instalacion del componente de Geolocalizacion.

npm install cordova-plugin-geolocation
npm install @ionic-native/geolocation
ionic cap sync

Una vez que hagamos esto, deberemos dirigirnos al archivo module.app y poner la siguiente linea para poder hacer uso de esta API dentro de nuestro proyecto

import { Geolocation } from '@ionic-native/geolocation/ngx';

En el mismo archivo dentro de imports, agregaremos Geolocation.

Luego crearemos un componente y definiremos los métodos que deseamos usar. (Ver componente gps).

Documentación oficial: https://ionicframework.com/docs/native/geolocation
